﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Windows.Speech;

public class KeyWordRecognizerBehaviour : MonoBehaviour
{
    string[] keywords = new string[] {"Go Up", "Go Down", "Turn Left", "Turn Right", "Stop"};
    //public ConfidenceLevel confidence = ConfidenceLevel.Low;

    float speed = 0.002f;
    float temp = 100.0f;

    protected PhraseRecognizer recognizer;
    protected string word = "";

    // Use this for initialization
    void Start()
    {
        if (keywords != null)
        {
            recognizer = new KeywordRecognizer(keywords);
            recognizer.OnPhraseRecognized += Recognizer_OnPhraseRecognized;
            recognizer.Start();
        }
    }

    private void Recognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        word = args.text;
        Debug.Log("You said: <b> " + args.text);
    }

    void Update()
    {
        var x = this.transform.position.x;
        var y = this.transform.position.y;
        var z = this.transform.position.z;

        switch (word)
        {
            case "Turn Right":
                x += speed;
                break;
            case "Turn Left":
                x -= speed;
                break;
            case "Go Up":
                y += speed;
                break;
            case "Go Down":
                y -= speed;
                break;
            case "Stop":
                var rb = this.gameObject.GetComponent<Rigidbody>();
                rb.velocity = Vector3.zero;
                break;
        }

        this.transform.position = new Vector3(x, y, z);
    }

    private void OnApplicationQuit()
    {
        if (recognizer != null && recognizer.IsRunning)
        {
            recognizer.OnPhraseRecognized -= Recognizer_OnPhraseRecognized;
            recognizer.Stop();
        }
    }

}
