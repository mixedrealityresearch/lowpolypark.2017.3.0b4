﻿using Academy.HoloToolkit.Unity;
using UnityEngine;

/// <summary>
/// GestureAction performs custom actions based on 
/// which gesture is being performed.
/// </summary>
public class GestureAction : MonoBehaviour
{
    [Tooltip("Rotation max speed controls amount of rotation.")]
    public float RotationSensitivity = 10.0f;
    public float MoveSpeed = 0.1f;
    public float RotateSpeed = 6f;
    public float ScaleSpeed = 0.2f;

    private Vector3 manipulationPreviousPosition;
    private float rotationFactor;

    void Update()
    {
        PerformRotation();
    }

    // NAVIGATION: Rotation
    private void PerformRotation()
    {
        /*if (GestureManager.Instance.IsNavigating &&
            (!ExpandModel.Instance.IsModelExpanded ||
            (ExpandModel.Instance.IsModelExpanded && HandsManager.Instance.FocusedGameObject == gameObject)))
        {
            // This will help control the amount of rotation.
            rotationFactor = GestureManager.Instance.NavigationPosition.x * RotationSensitivity;

            transform.Rotate(new Vector3(0, -1 * rotationFactor, 0));
        }*/
        if (GestureManager.Instance.IsNavigating && HandsManager.Instance.FocusedGameObject == gameObject)
        {
            // 2.c: Calculate rotationFactor based on GestureManager's NavigationPosition.X and multiply by RotationSensitivity.
            // This will help control the amount of rotation.
            rotationFactor = GestureManager.Instance.NavigationPosition.x * RotationSensitivity;

            // 2.c: transform.Rotate along the Y axis using rotationFactor.
            transform.Rotate(new Vector3(0, -1 * rotationFactor, 0));
        }
    }

    /// <summary>
    /// START MANIPULATION
    /// </summary>
    void PerformManipulationStart(Vector3 position)
    {
        manipulationPreviousPosition = position;
    }
    void ScaleManipulationStart(Vector3 position)
    {
        manipulationPreviousPosition = position;
    }

    /// <summary>
    /// UPDATE MANIPULATION
    /// </summary>
    void PerformManipulationUpdate(Vector3 position)
    {
        /*if (GestureManager.Instance.IsManipulating)
        {
            Vector3 moveVector = Vector3.zero;
            moveVector = position - manipulationPreviousPosition;
            manipulationPreviousPosition = position;

            transform.position += moveVector;
        }*/
        if (GestureManager.Instance.IsManipulating)
        {
            Debug.Log("You are moving the cube.");
            Vector3 moveVector = Vector3.zero;

            // 4.a: Calculate the moveVector as position - manipulationPreviousPosition.
            moveVector = position - manipulationPreviousPosition;

            // 4.a: Update the manipulationPreviousPosition with the current position.
            manipulationPreviousPosition = position;

            // 4.a: Increment this transform's position by the moveVector.
            transform.position += moveVector;
        }
    }

    void ScaleManipulationUpdate(Vector3 position)
    {
        if (GestureManager.Instance.IsManipulating)
        {
            Debug.Log("You are scaling the cube.");
            transform.localScale += new Vector3(0.1f * ScaleSpeed, 0, 0);
        }
    }

    /*public enum ManipulationType { Position = 0, Scale = 1 };
    private bool _manipulating;
    private Vector3 _lastManipulationPosition;
    private ManipulationType _manipulationType;

    public void StartLocationManipulation(Vector3 pos)
    {
        _manipulationType = ManipulationType.Position;
        StartManipulation(pos);
    }

    void Update()
    {
        if (_manipulating)
        {
            UpdateManipulation();
            _manipulating = GestureManager.Instance.IsManipulating;
        }
    }

    public void StartScaleManipulation(Vector3 pos)
    {
        _manipulationType = ManipulationType.Scale;
        StartManipulation(pos);
    }

    private void StartManipulation(Vector3 pos)
    {
        _lastManipulationPosition = pos;
        _manipulating = true;
    }

    public void UpdateManipulation()
    {
        Vector3 pos = GestureManager.Instance.ManipulationPosition;

        Vector3 diff = _lastManipulationPosition - pos;

        switch (_manipulationType)
        {
            case (ManipulationType.Scale):
                Vector3 currentScale = transform.localScale;
                currentScale += diff;
                transform.localScale = currentScale;
                break;
            case (ManipulationType.Position):

                Vector3 currentPos = transform.position;
                currentPos -= diff;
                transform.position = currentPos;
                break;
        }

        _lastManipulationPosition = pos;

        _manipulating = GestureManager.Instance.IsManipulating;
    }*/
}