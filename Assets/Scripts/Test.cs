﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {

    public float fallSpeed = 8.0f;
    public float spinSpeed = 250.0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.down * fallSpeed * Time.deltaTime, Space.World);
        transform.Rotate(Vector3.forward, spinSpeed * Time.deltaTime);

        //Debug.Log("RIGIDBDODY: " + gameObject.GetComponent<Rigidbody>().name);
        //Debug.Log("COLLIDER: " + gameObject.GetComponent<Collider>().name);
    }
}
