﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var soundManager = GameObject.Find("Audio Manager");
        TextToSpeechManager textToSpeech = soundManager.GetComponent<TextToSpeechManager>();
        textToSpeech.Voice = TextToSpeechVoice.Mark;
        textToSpeech.SpeakText("Welcome to the Holographic App ! This is a 3D LowPoly Park example. Enjoy and have a good day!");
    }

    // Update is called once per frame
    void Update()
    {

    }
}